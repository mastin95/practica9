package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;

public class MyGdxGame extends ApplicationAdapter implements InputProcessor {
	SpriteBatch batch;
	Texture img;
	ParticleEffect pe;
	TiledMapRenderer tiledMapRenderer;
	TiledMap tiledMap;

	OrthographicCamera camera;
	
	@Override
	public void create () {

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,w,h);
		camera.update();
		batch = new SpriteBatch();
		pe = new ParticleEffect();



		pe.load(Gdx.files.internal("Particles/particle.p"),Gdx.files.internal(""));
	    pe.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
		pe.start();
		Gdx.input.setInputProcessor((InputProcessor) this);
		tiledMap = new TmxMapLoader().load("Map1.tmx");
		tiledMapRenderer = new OrthoCachedTiledMapRenderer(tiledMap);
		img = new Texture("terrain.png");

		pe.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);

		pe.start();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();
		batch.begin();
		pe.draw(batch,Gdx.graphics.getDeltaTime());
		batch.end();
	}
	@Override
	public boolean keyDown(int keycode)
	{
		if(keycode== Input.Keys.LEFT)
		{
			camera.translate(-32,0);
		}
		if(keycode== Input.Keys.UP)
		{
			camera.translate(0,-32);
		}
		if(keycode== Input.Keys.RIGHT)
		{
			camera.translate(0,32);
		}
		if(keycode== Input.Keys.DOWN)
		{
			camera.translate(-32,0);
		}
		if(keycode == Input.Keys.NUM_2)
		{
			pe.scaleEffect(2);
		}
		if(keycode == Input.Keys.NUM_1)
		{
			pe.scaleEffect(0.2f);
		}
		if(keycode == Input.Keys.NUM_3)
		{
			pe.scaleEffect(3);
		}
		if(keycode == Input.Keys.NUM_4)
		{
			pe.scaleEffect(4);
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		pe.setPosition(screenX,Gdx.graphics.getHeight()-screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
